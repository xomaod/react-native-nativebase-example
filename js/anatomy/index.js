import React, { Component } from 'react';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Icon, Badge, List, ListItem, Grid, Col, Row, 
  Picker, Item, Radio, Card, CardItem, Thumbnail } from 'native-base';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';

export default class AwesomeNativeBase extends Component {
  
  constructor(props) {
        super(props);
        this.state = {
            selectedItem: undefined,
            selected1: 'key1',
            results: {
                items: []
            }
        }
    }
  
  onValueChange (value: string) {
        this.setState({
            selected1 : value
        });
  }


  render() {
    return (
            <Container> 
                <Header style={{ height:60 }}>

                    <Button transparent>
                        <Icon name='ios-arrow-back' />
                    </Button>
                    
                    <Title>Aoddy Test NativeBase</Title>
                    
                    <Button transparent>
                        <Icon name='ios-menu' />
                    </Button>
                </Header>

                <Content style={{padding:10}}>

                    <Card>

                        <CardItem >                       
                             <Thumbnail source={require('../../img/nativeBase-logo.png')} />
                            <Text>NativeBase</Text>
                            <Text note>April 15, 2016</Text>
                        </CardItem>

                        <CardItem cardBody> 
                            <Image style={{ resizeMode: 'cover' }} source={require('../../img/wave.png')} /> 
                            <Text>
                                Want to have something more with Card Lists? 
Include image with CardItem within Card along with some text before and after image to create a card with lists. 
Here is your Card Image ready !
                  
                                Want to have something more with Card Lists? 
Include image with CardItem within Card along with some text before and after image to create a card with lists. 
Here is your Card Image ready !
                            </Text>
                            <Button transparent textStyle={{color: '#87838B'}}>
                                389 Stars
                            </Button>
                        </CardItem>

                   </Card>

                   <Text></Text>

                   <Card>

                        <CardItem >                       
                             <Thumbnail source={require('../../img/nativeBase-logo.png')} />
                            <Text>NativeBase</Text>
                            <Text note>April 15, 2016</Text>
                        </CardItem>

                        <CardItem cardBody> 
                            <Image style={{ resizeMode: 'cover' }} source={require('../../img/grass.png')} /> 
                            <Text>
                                Want to have something more with Card Lists? 
Include image with CardItem within Card along with some text before and after image to create a card with lists. 
Here is your Card Image ready !
                  
                                Want to have something more with Card Lists? 
Include image with CardItem within Card along with some text before and after image to create a card with lists. 
Here is your Card Image ready !
                            </Text>
                            <Button transparent textStyle={{color: '#87838B'}}>
                                389 Stars
                            </Button>
                        </CardItem>

                   </Card>

                   <Text></Text>

                   <Card>

                        <CardItem >                       
                             <Thumbnail source={require('../../img/nativeBase-logo.png')} />
                            <Text>NativeBase</Text>
                            <Text note>April 15, 2016</Text>
                        </CardItem>

                        <CardItem cardBody> 
                            <Image style={{ resizeMode: 'cover' }} source={require('../../img/snowflakes.png')} /> 
                            <Text>
                                Want to have something more with Card Lists? 
Include image with CardItem within Card along with some text before and after image to create a card with lists. 
Here is your Card Image ready !
                  
                                Want to have something more with Card Lists? 
Include image with CardItem within Card along with some text before and after image to create a card with lists. 
Here is your Card Image ready !
                            </Text>
                            <Button transparent textStyle={{color: '#87838B'}}>
                                389 Stars
                            </Button>
                        </CardItem>

                   </Card>

                   <Text></Text>

                   <Card>

                        <CardItem >                       
                             <Thumbnail source={require('../../img/nativeBase-logo.png')} />
                            <Text>NativeBase</Text>
                            <Text note>April 15, 2016</Text>
                        </CardItem>

                        <CardItem cardBody> 
                            <Image style={{ resizeMode: 'cover' }} source={require('../../img/wave.png')} /> 
                            <Text>
                                Want to have something more with Card Lists? 
Include image with CardItem within Card along with some text before and after image to create a card with lists. 
Here is your Card Image ready !
                  
                                Want to have something more with Card Lists? 
Include image with CardItem within Card along with some text before and after image to create a card with lists. 
Here is your Card Image ready !
                            </Text>
                            <Button transparent textStyle={{color: '#87838B'}}>
                                389 Stars
                            </Button>
                        </CardItem>

                   </Card>

                </Content>

                <Footer style={{ height:60 }}>
                    <FooterTab>

                        <Button style={{ height:50, width:50 }}>
                            Refresh
                            <Icon name='ios-refresh-outline' />
                        </Button>
                        
                    </FooterTab>
                </Footer>
            </Container>
    );
  }


}

AppRegistry.registerComponent('AwesomeNativeBase', () => AwesomeNativeBase);