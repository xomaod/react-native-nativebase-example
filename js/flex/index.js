import React, { Component } from 'react';
import { AppRegistry, View, Text } from 'react-native';

export default class FlexDimensionsBasics extends Component {
  render() {
    return (
      // Try removing the `flex: 1` on the parent View.
      // The parent will not have dimensions, so the children can't expand.
      // What if you add `height: 300` instead of `flex: 1`?
      <View style={{flex: 1}}>
        <View style={{flex: 3, backgroundColor: 'powderblue'}} >
          <View style={{flex: 1}}>
            <Text>View 1.1</Text>
          </View>
          <View style={{flex: 1}}>
            <Text>View 1.2</Text>
          </View>
        </View>
        <View style={{flex: 2, backgroundColor: 'skyblue'}} ><Text>View 2</Text></View>
        <View style={{flex: 1, backgroundColor: 'steelblue'}} ><Text>View 3</Text></View>
      </View>

    );
  }
};